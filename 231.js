      <Carousel className="col-12 col-md-5 mx-auto">
        <Carousel.Item interval={1000} className="carouselDiv text-center">
          <img
            className="img-fluid h-100"
            src="/images/xmasballs1.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={500} className="carouselDiv text-center">
          <img
            className="img-fluid h-100"
            src="/images/xmasSantaMama.jpg"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className="carouselDiv text-center">
          <img
            className="img-fluid h-100"
            id="standCrsl"
            src="/images/xmasStand.jpg"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>


              <CardDeck className="my-md-3">
          <Card className="col-md-3 bg-dark text-white p-0">
            <Card.Img src="/images/xmasballs1.jpg" alt="Card image" />
          </Card>
        </CardDeck>
        <CardDeck className="my-md-3">
          <Card className="col-md-4 ml-auto bg-dark text-white p-0">
            <Card.Img src="/images/xmasStand.jpg" alt="Card image" />
          </Card>
        </CardDeck>
        <CardDeck className="my-md-3">
          <Card className="col-md-3 bg-dark text-white p-0">
            <Card.Img src="/images/xmasSantaMama.jpg" alt="Card image" />
          </Card>
        </CardDeck>


          font-family: 'Nanum Myeongjo', serif;
          font-family: 'Questrial', sans-serif;
          font-family: 'Nanum Myeongjo', serif;