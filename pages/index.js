import { Fragment, useState } from 'react'
import Head from 'next/head'
import Image from 'next/image'
import Carousel from 'react-bootstrap/Carousel'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import {CardDeck, Card, Modal} from 'react-bootstrap'
import { FaFacebookSquare, FaInstagramSquare, FaTwitterSquare } from "react-icons/fa";

export default function Home() {

    const [show1,setShow1]=useState(false)
  const [itemName, setItemName] = useState('')
  const [image, setImage] = useState('')

  const handleShow = (param1, param2) => {

    setItemName(param1)
    setImage(param2)
    setShow1(true)

  }

  const handleClose = () => {

    setItemName("")
    setImage("")
    setShow1(false)
  }
  return (
    <Fragment>
    <Head>
      <title>Ferdie's Emporium | Home</title>
    </Head>
    <div className="container d-md-flex flex-md-column justify-content-md-center">
      <div className="text-center text-white mt-4">
            <h1 className="display-1 d-none d-md-block">Ferdie's Xmas Emporium</h1>
            <h1 className="d-block d-md-none">Ferdie's Xmas Emporium</h1>
            <p>
              View our Handcrafted and Handpainted Christmas Ornaments or Contact us for more details!
            </p>
            <div className="my-3">
              <span className="mx-2"><FaFacebookSquare size={30}/> </span>
              <span className="mx-2"><FaInstagramSquare size={30}/> </span>
              <span className="mx-2"><FaTwitterSquare size={30}/>  </span>
            </div>

            <p>
              <a href="/items"className="btn btn-outline-light mx-2">Visit Gallery</a>
              <a href="/about"className="btn btn-outline-light mx-2">Contact Us!</a>
            </p>
      </div>
      <div className="col-md-6 mt-3 mb-5 mx-auto">
          <CardDeck>
            <Card className="bg-dark text-white p-0" onClick={()=>handleShow("Santa and Mama Santa", "/images/xmasSantaMama.jpg")}>
              <Card.Img src="/images/xmasSantaMama.jpg" alt="Card image" />
            </Card>
            <Card className="bg-dark text-white p-0" onClick={()=>handleShow("Xmas Balls", "/images/xmasballs1.jpg")}>
              <Card.Img src="/images/xmasballs1.jpg" alt="Card image" />
            </Card>
            <Card className="bg-dark text-white p-0" onClick={()=>handleShow("Santa Cookie", "/images/santaCookie.jpg")}>
              <Card.Img src="/images/santaCookie.jpg" alt="Card image" />
            </Card>
          </CardDeck>
        </div>
      </div>
      <Modal show={show1} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{itemName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <img src={image} alt={itemName} className="img-fluid"/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      
    </Fragment>

  )
}
