import '../styles/globals.css'
import Head from 'next/head'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'

function MyApp({ Component, pageProps }) {
  return (
  		<>	
  			<Head>
  			<link rel="preconnect" href="https://fonts.gstatic.com"/>
  			<link href="https://fonts.googleapis.com/css2?family=Cinzel&family=Quattrocento&family=Quattrocento+Sans&display=swap" rel="stylesheet"/>	
  			</Head>
  			<NavBar />
  			<Container fluid className="mb-5">
		  	<Component {...pageProps} />
  			</Container>
  			<Footer />	
  		</>

  	)

}

export default MyApp
