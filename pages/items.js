import NavBar from 	"../components/NavBar"
import Gallery from "../components/Gallery"
import Head from 'next/head'

export default function items(){

	return (
			<div className="container mt-5">
				<Head>
				  <title>Ferdie's Emporium | Gallery</title>
				</Head>
				<Gallery />
			</div>

		)

}