import { FaFacebookSquare, FaInstagramSquare, FaTwitterSquare,FaArrowCircleDown, FaPhone } from "react-icons/fa";
import { AiOutlineMail, AiTwotoneMail } from "react-icons/ai";

export default function about(){

	return (

			<>
			<div className="row">
				<div className="col-md-6 mx-auto">
					<div className="row">
						<div className="col-6 col-md-3 my-5 my-md-3 mx-auto p-1 bg-light rounded-circle ">
							<img src="/images/ferdie.jpg" className="img-fluid border border-light rounded-circle " alt=""/>					
						</div>
					</div>
					<div className="row">
						<div className="text-center text-white mx-auto mt-3">
							<h1 id="ferdieH1">Ferdinand L. Calinao</h1>
							<h3>Artist/Sculptor</h3>
							<p>
								Ferdinand L. Calinao has been an artist for more than 30 years. He previously worked as a main sculptor/artist for a firm called Uysons. He is now retired but continues to create scupltures and figurines.
							</p>
						</div>
						<div className="text-white mx-auto my-3">
						  <span className="mx-2"><FaFacebookSquare size={30}/> </span>
						  <span className="mx-2"><FaInstagramSquare size={30}/> </span>
						  <span className="mx-2"><FaTwitterSquare size={30}/>  </span>
						</div>
					</div>
					<div className="row">
						<div className="mx-auto mt-3 bounce">
							<a href="#contact">
						    	<FaArrowCircleDown size={30}/>
							</a>
						</div>
					</div>
					<div className="row">
						<div className="text-center text-white mx-auto mt-3" id="contact">
							<h1>Contact Us!</h1>
							<div className="mx-auto my-3">
							    <FaPhone size={30}/>
							</div>
							<h3>Phone Numbers</h3>
							<p>
								09266772400
							</p>
							<div className="mx-auto my-3">
							    <AiTwotoneMail size={30}/>
							</div>
							<h3>Email</h3>
							<p>
								teejaeism@gmail.com
							</p>
							<p>
								bong_cha@gmail.com
							</p>
						</div>
					</div>
				</div>
			</div>
			</>
		)

}