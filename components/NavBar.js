import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
//import nextJS Link component for client-side navigation
import Link from 'next/link'


export default function NavBar() {
    return (
        <Navbar variant="dark" expand="lg" className="mb-md-5 ">
            <Link href="/">
                <a className="navbar-brand d-lg-none">Ferdie's Emporium</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mx-auto text-center">

                            <Link href="/items">
                                <a className="nav-link mr-lg-auto" role="button">Gallery</a>
                            </Link>
                            <Link href="/" className="">
                                <a className="navbar-brand d-none d-lg-block mx-5">Ferdie's Emporium</a>
                            </Link>
                            <Link href="/about">
                                <a className="nav-link ml-lg-auto" role="button">Contact Us!</a>
                            </Link>

                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}