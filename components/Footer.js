import { GiTechnoHeart } from "react-icons/gi";
export default function footer(){


	return (

			<footer className="d-flex align-items-center justify-content-center mt-5 p-2">
				<div className="text-black text-center">
					Made with <GiTechnoHeart size={20}/> by <a href="https://friendly-kilby-36034c.netlify.app" target="_blank">Tee Jae Bengan Calinao</a>
				</div>
			</footer>


		)

}