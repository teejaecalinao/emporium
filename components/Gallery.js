import {useState} from 'react'
import {Card, CardGroup, CardDeck, Modal, Button} from 'react-bootstrap'
import { FaFacebookSquare, FaInstagramSquare, FaTwitterSquare } from "react-icons/fa";
export default function gallery({hello}){

	const [show1,setShow1]=useState(false)
	const [itemName, setItemName] = useState('')
	const [image, setImage] = useState('')

	const handleShow = (param1, param2) => {

		setItemName(param1)
		setImage(param2)
		setShow1(true)

	}

	const handleClose = () => {

		setItemName("")
		setImage("")
		setShow1(false)
	}

	return (
			<>
			<div className="text-white text-center mt-2 mb-4">
				<h1 className="display-1 d-none d-md-block">Gallery</h1>
				<h1 className="d-block d-md-none">Gallery</h1>
				<h2 className="my-3">Handcrafted Christmas Ornaments</h2>
				<div className="my-3">
				  <span className="mx-2"><FaFacebookSquare size={30}/> </span>
				  <span className="mx-2"><FaInstagramSquare size={30}/> </span>
				  <span className="mx-2"><FaTwitterSquare size={30}/>  </span>
				</div>
			</div>
			<CardDeck className="my-md-3">
				<Card className="bg-dark text-white mr-2" onClick={()=>handleShow("Christmas Balls", "/images/allBalls.jpg")}>
				  <Card.Img src="/images/allBalls.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Christmas Balls</Card.Title>
				  </Card.ImgOverlay>
				</Card>
			</CardDeck>
			<CardDeck>
				<Card className="bg-dark text-white mr-2" onClick={()=>handleShow("Mama Santa", "/images/mamaSanta.jpg")}>
				  <Card.Img src="/images/mamaSanta.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Mama Santa</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa And Rudolf", "/images/xmasRdlfSnta.jpg")}>
				  <Card.Img src="/images/xmasRdlfSnta.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa and Rudolf</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa And Mama Santa", "/images/SantaandMamaSanta.jpg")}>
				  <Card.Img src="/images/SantaandMamaSanta.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa and Mama Santa</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Snowman ", "/images/snowMan.jpg")}>
				  <Card.Img src="/images/snowMan.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Snowman</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Doraemon on Sleigh", "/images/doraemonAndSleigh.jpg")}>
				  <Card.Img src="/images/doraemonAndSleigh.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Doraemon on Sleigh</Card.Title>
				  </Card.ImgOverlay>
				</Card>
			</CardDeck>
			<CardDeck className="mt-md-3">
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa's Sleigh'", "/images/sleigh.jpg")}>
				  <Card.Img src="/images/sleigh.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa's Sleigh</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mr-2" onClick={()=>handleShow("Ornament Tree", "/images/xmasStand.jpg")}>
				  <Card.Img src="/images/xmasStand.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Ornament Tree</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa on Sleigh", "/images/santaOnSleigh.png")}>
				  <Card.Img src="/images/santaOnSleigh.png" className="h-100" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa's Sleigh</Card.Title>
				  </Card.ImgOverlay>
				</Card>

			</CardDeck>
			<CardDeck className="my-md-3">
			<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Xmas Balls", "/images/xmasballs1.jpg")}>
			  <Card.Img src="/images/xmasballs1.jpg" alt="Card image" />
			  <Card.ImgOverlay>
			    <Card.Title>Christmas Balls</Card.Title>
			  </Card.ImgOverlay>
			</Card>
			</CardDeck>
			<CardDeck className="mt-md-3 mb-md-5">
				<Card className="bg-dark text-white mr-2" onClick={()=>handleShow("Elf", "/images/elf1.jpg")}>
				  <Card.Img src="/images/elf1.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Elf</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa Cookie", "/images/santaCookie.jpg")}>
				  <Card.Img src="/images/santaCookie.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa Cookie</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Rudolf", "/images/rudolf.jpg")}>
				  <Card.Img src="/images/rudolf.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Rudolf</Card.Title>
				  </Card.ImgOverlay>
				</Card>
				<Card className="bg-dark text-white mx-2" onClick={()=>handleShow("Santa and Mama Santa with Elf", "/images/xmasSantaMama.jpg")}>
				  <Card.Img src="/images/xmasSantaMama.jpg" alt="Card image" />
				  <Card.ImgOverlay>
				    <Card.Title>Santa, Mama Santa and Elf</Card.Title>
				  </Card.ImgOverlay>
				</Card>
			</CardDeck>

			<Modal show={show1} onHide={handleClose}>
			  <Modal.Header closeButton>
			    <Modal.Title >{itemName}</Modal.Title>
			  </Modal.Header>
			  <Modal.Body>
			  	<img src={image} alt={itemName} className="img-fluid"/>
			  </Modal.Body>
			  <Modal.Footer>
			    <Button variant="secondary" onClick={handleClose}>
			      Close
			    </Button>
			  </Modal.Footer>
			</Modal>
			</>
		)

}